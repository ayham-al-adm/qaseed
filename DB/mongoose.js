const mongoose = require('mongoose')
const conUrl = '127.0.0.1:27017';
mongoose.set('useCreateIndex', true);
const dbURL = process.env.MONGODB_URI || 'mongodb://' + conUrl + `/qaseed-db`;
mongoose.connect(dbURL, {
    "useNewUrlParser": true,
    "useUnifiedTopology": true
});

mongoose.connection.on('connected', () => {
    console.log("connected");
});

mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));