const router = require('express').Router();
const blockController = require('../controllers/block.controller');
const jwtMiddleware = require('../middlewares/jwt.middleware');

router.use(jwtMiddleware);

router.post('/', async (req, res, next) => {
    try {
        await blockController.add(req.user._id, req.body.userId);
        res.status(201).json("done");
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.delete('/', async (req, res, next) => {
    try {
        await blockController.remove(req.user._id, req.query.userId);
        res.status(200).json("done");
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/', async (req, res, next) => {
    try {
        let blocks = await blockController.getBlockingUsers(req.user._id, req.query.page);
        res.status(200).json(blocks);
    } catch (e) {
        res.status(500).json(e.message);
    }
});


module.exports = router;