const router = require('express').Router();
const otherController = require('../controllers/others.controller');
const jwtMiddleware = require('../middlewares/jwt.middleware');


router.get('/poems', jwtMiddleware, async (req, res, next) => {
    try {
        let posts = await otherController.getOtherPoems(req.query.page, req.query.otherId, req.user._id);
        res.status(200).json(posts);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/followers', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await otherController.getOtherFollowers(req.query.page, req.query.otherId, req.user._id);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/following', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await otherController.getOtherFollowers(req.query.page, req.query.otherId, req.user._id);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/', jwtMiddleware, async (req, res, next) => {
    try {
        let user = await otherController.getOtherProfile(req.query.otherId, req.user._id);
        res.status(200).json(user);
    } catch (e) {
        res.status(500).json(e.message);
    }
});


module.exports = router;