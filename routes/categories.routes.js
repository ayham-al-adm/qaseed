const router = require('express').Router();
const categoryController = require('../controllers/category.controller');

router.get('/user', async (req, res, next) => {
    try {
        let cats = await categoryController.getUserCategories();
        res.status(200).json(cats);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/post', async (req, res, next) => {
    try {
        let cats = await categoryController.getPostCategories();
        res.status(200).json(cats);
    } catch (e) {
        res.status(500).json(e.message);
    }
});


router.post('/', async (req, res, next) => {
    try {
        let cat = await categoryController.addNewCategory(req.body);
        res.status(201).json(cat);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

module.exports = router;