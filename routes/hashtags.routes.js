const router = require('express').Router();
const hashtagsController = require('../controllers/hashtag.controller');


router.post('/', async (req, res, next) => {
    try {
        let hash = await hashtagsController.addHshtagToDatabase(req.body);
        res.status(201).json(hash);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/', async (req, res, next) => {
    try {
        let hashs = await hashtagsController.getHashtagsByKey(req.query.key);
        res.status(200).json(hashs);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

module.exports = router;
