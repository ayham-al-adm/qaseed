const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const jwtMiddleware = require('../middlewares/jwt.middleware');
const followingController = require('../controllers/following.controller');
const mailerController = require('../controllers/mailer.controller');

router.post('/register', async (req, res, next) => {
    try {
        return userController.logUser(req, res);
    }
    catch (e) {
        console.log(e.message);
    }
});

router.put('/edit', jwtMiddleware, async (req, res, next) => {
    try {
        let result = await userController.edit(req.user._id, req.body.fcmToken, req.body.image, req.body.name, req.body.language, req.body.bio, req.body.isPoet, req.body.birthdate, req.body.country, req.body.city, req.body.otherAdress, req.body.category);
        res.status(200).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/follow', jwtMiddleware, async (req, res, next) => {
    try {
        let result = await followingController.addFollow(req.user._id, req.body.userId);
        if (result == true)
            res.status(201).json(result);
        else {
            res.status(500).json(result);
        }
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.delete('/follow', jwtMiddleware, async (req, res, next) => {
    try {
        await followingController.removeFollow(req.user._id, req.query.userId);
        res.status(200).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/following', jwtMiddleware, async (req, res, next) => {
    try {
        let following = await followingController.getFollowing(req.user._id, req.query.page);
        res.status(200).json(following);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/followers', jwtMiddleware, async (req, res, next) => {
    try {
        let followers = await followingController.getFollowers(req.user._id, req.query.page);
        res.status(200).json(followers);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/refresh-token', async (req, res, next) => {
    try {
        const newToken = await userController.refreshJWTToken(req.headers.authorization.split(' ')[1]);
        res.status(200).json({ token: newToken });
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/suggest-users', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await followingController.getSuggestedUsers(req.user._id, req.query.page);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/profile', jwtMiddleware, async (req, res, next) => {
    try {
        let user = await userController.getUserProfile(req.user._id);
        res.status(200).json(user);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/signup-with-email', async (req, res, next) => {
    try {
        const result = await userController.signup(req.body.email, req.body.password);
        res.status(201).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/login-with-email', async (req, res) => {
    try {
        const result = await userController.login(req.body.email, req.body.password);
        if (result === "NOTVERIFIED") {
            return res.status(401).json("Verify your email please!");
        }
        return res.status(200).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/test-mail', async (req, res, next) => {
    try {
        //let result = process.env.EMAIL_PASSWORD;
        let result = await mailerController.sentVerificationMail("ayham.alazm@gmail.com", "122522");
        res.status(200).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.put('/verify-email', async (req, res) => {
    try {
        let modifiedCount = await userController.verifyEmail(req.query.email, req.query.code);
        if (modifiedCount == 0) {
            res.status(401).json("not valid email and/or code");
        } else {
            res.status(200).json("verified");
        }
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/send-verification-code', async (req, res, next) => {
    try {
        let result = await userController.sentVerificationCode(req.body.email);
        res.status(201).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/reset-password', async (req, res, next) => {
    try {
        let result = await userController.resetPassword(req.body.email, req.body.code, req.body.newPassword);
        res.status(200).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

module.exports = router;