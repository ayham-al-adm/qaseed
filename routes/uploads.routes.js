const express = require('express');
const router = express.Router();
const multer = require("multer");
const path = require('path');
const uniqueString = require('unique-string');
const maxSize = 2 * 1024 * 1024;

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __dirname + "/../" + "/images/");
    },
    filename: (req, file, cb) => {
        console.log(file.originalname);
        cb(null, file.originalname);
    },
});

let storageWithGenerateName = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __dirname + "/../" + "/images/");
    },
    filename: (req, file, cb) => {
        cb(null, uniqueString() + path.extname(file.originalname));
    },
});

let uploadFile = multer({
    storage: storage,
    limits: { fileSize: maxSize },
}).single("file");

let uploadFileWithoutName = multer({
    storage: storageWithGenerateName,
    limits: { fileSize: maxSize },
}).single("file");

router.post('/image', async (req, res, next) => {
    try {
        await uploadFile(req, res, () => {
            res.status(201).json(true);
        });
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/image-without-name', async (req, res, next) => {
    try {
        await uploadFileWithoutName(req, res, () => {
            res.status(201).json(req.file.filename);
        });
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.use(express.static(path.join(__dirname, "..", 'images')));

module.exports = router;