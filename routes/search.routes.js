const router = require('express').Router();
const searchController = require('../controllers/search.controller');
const jwtMiddleware = require('../middlewares/jwt.middleware');
router.get('/users', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await searchController.searchUsers(req.query.keyword, req.query.page, req.user._id);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/users-by-category', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await searchController.searchUsersByCategory(req.query.keyword, req.query.page, req.user._id, req.query.categoryId);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/poems', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await searchController.searchPoems(req.query.keyword, req.query.page, req.user._id);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/poems-by-category', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await searchController.searchPoemsByCategory(req.query.keyword, req.query.page, req.user._id, req.query.categoryId);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/my-poems', jwtMiddleware, async (req, res, next) => {
    try {
        let users = await searchController.searchMyPoems(req.query.keyword, req.query.page, req.user._id);
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

module.exports = router;