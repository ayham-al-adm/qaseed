const router = require('express').Router();
const notificationController = require('../controllers/notification.controller');
const jwtMiddlewarer = require('../middlewares/jwt.middleware');


router.get('/new-count', jwtMiddlewarer, async (req, res, next) => {
    try {

        let nots = await notificationController.getNewNotificationsCount(req.user._id);
        res.status(200).json(nots);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.patch('/mark-as-read', jwtMiddlewarer, async (req, res, next) => {
    try {

        let nots = await notificationController.getNewNotificationsCount(req.user._id);
        res.status(200).json(nots);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/', jwtMiddlewarer, async (req, res, next) => {
    try {

        let nots = await notificationController.getUserNotifications(req.user._id, req.query.page);
        res.status(200).json(nots);
    } catch (e) {
        res.status(500).json(e.message);
    }
});


module.exports = router;