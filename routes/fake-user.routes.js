const fakeController = require('../controllers/fake-user.controller');

const router = require('express').Router();

router.post('/add-user', async (req, res, next) => {
    try {
        let user = await fakeController.addFakeUser(req.body);
        res.status(201).json(user);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/get-all', async (req, res, next) => {
    try {
        let users = await fakeController.getAllFakeUsers();
        res.status(200).json(users);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/add-poem', async (req, res, next) => {
    try {
        let poem = await fakeController.addPoem(req.body);
        res.status(201).json(poem);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

module.exports = router;