const router = require('express').Router();
const jwtMiddleware = require('../middlewares/jwt.middleware');
const storyController = require('../controllers/story.controller');



router.post('/like', jwtMiddleware, async (req, res, next) => {
    try {
        let story = await storyController.addLike(req.body.storyId, req.user._id);
        res.status(201).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.delete('/like', jwtMiddleware, async (req, res, next) => {
    try {
        await storyController.removeLike(req.query.storyId, req.user._id);
        res.status(200).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/like', jwtMiddleware, async (req, res, next) => {
    try {
        let likes = await storyController.getLikes(req.query.storyId, req.query.page, req.user._id);
        res.status(200).json(likes);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/', jwtMiddleware, async (req, res, next) => {
    try {
        let stories = await storyController.getMainAggregatedStories(req.user._id, req.query.page);
        res.status(200).json(stories);
    } catch (e) {
        res.status(500).json(e.message);
    }
});


router.post('/', jwtMiddleware, async (req, res, next) => {
    try {
        let story = await storyController.add(req.body, req.user._id);
        res.status(201).json(story);
    } catch (e) {
        res.status(500).json(e.message);
    }
});


module.exports = router;