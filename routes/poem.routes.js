const router = require('express').Router();
const poemController = require('../controllers/poem.controller');
const jwtMiddleware = require('../middlewares/jwt.middleware');



router.get('/main', jwtMiddleware, async (req, res, next) => {
    try {
        let poems = await poemController.getMainPosts(req.user._id, req.query.page);
        res.status(200).json(poems);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/like', jwtMiddleware, async (req, res, next) => {
    try {
        let poem = await poemController.addLike(req.body.poemId, req.user._id);
        res.status(201).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.delete('/like', jwtMiddleware, async (req, res, next) => {
    try {
        let likes = await poemController.removeLike(req.query.poemId, req.user._id);
        res.status(200).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/like', jwtMiddleware, async (req, res, next) => {
    try {
        let likes = await poemController.getLikes(req.query.poemId, req.query.page, req.user._id);
        res.status(200).json(likes);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/favorite', jwtMiddleware, async (req, res, next) => {
    try {
        let result = await poemController.addToFavorites(req.body.poemId, req.user._id);
        res.status(201).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/favorite', jwtMiddleware, async (req, res, next) => {
    try {
        let poems = await poemController.getFavoritePoems(req.user._id, req.query.page);
        res.status(200).json(poems);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.delete('/favorite', jwtMiddleware, async (req, res, next) => {
    try {
        let result = await poemController.deleteToFavorites(req.query.poemId, req.user._id);
        res.status(200).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/my-poems', jwtMiddleware, async (req, res, next) => {
    try {
        let poems = await poemController.getMyPoems(req.query.page, req.user._id);
        res.status(200).json(poems);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.get('/today', jwtMiddleware, async (req, res, next) => {
    try {
        let poems = await poemController.getTodayPoem(req.user._id);
        res.status(200).json(poems);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post("/inc-views-count", jwtMiddleware, async (req, res, next) => {
    try {
        let result = await poemController.incViewsCount(req.body.poemId);
        res.status(200).json(result);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/edit', jwtMiddleware, async (req, res, next) => {
    try {
        let poem = await poemController.edit(req.user._id, req.body.poemId, req.body.name, req.body.hashtags, req.body.caption, req.body.verses, req.body.backgroundImage, req.body.backgroundColor, req.body.versesType)
        res.status(200).json(poem);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.delete('/', jwtMiddleware, async (req, res, next) => {
    try {
        let result = await poemController.delete(req.user._id, req.query.poemId);
        res.status(200).json(true);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

router.post('/', jwtMiddleware, async (req, res, next) => {
    try {
        let poem = await poemController.add(req.body, req.user._id);
        res.status(201).json(poem);
    } catch (e) {
        res.status(500).json(e.message);
    }
});

module.exports = router;