const port = process.env.PORT || 3000;
const app = require('./app');
require('./DB/mongoose');
//setup .env File
require('dotenv').config();
app.listen(port);
console.log(port);
