const hashModel = require('../models/hashtag.model');

module.exports = class Hashtag {
    static async getHashtagsByKey(key) {
        let hashs = await hashModel.find({ text: { $regex: key } }).sort({ date: - 1, });
        return hashs;
    }

    static async addHshtagToDatabase(hashData) {
        let hash = new hashModel({ ...hashData, date: new Date() });
        await hash.save();
        return hash;
    }

    static async upgradeHashtags(listOfHashs) {
        let result = await hashModel.updateMany({ _id: { $in: listOfHashs } }, { date: new Date(), $inc: { usesCount: 1 } });
        return true;
    }
}