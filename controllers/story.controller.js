const storyModel = require('../models/story.model');
const userModel = require('../models/user.model');
const blockController = require('./block.controller');
const notificationController = require('./notification.controller');
const getIDsAsObjectID = require('../utils/getIDsFromFollowingAsObjectID');
const mongoose = require('mongoose');

module.exports = class Story {
    static async add(body, owner) {
        const addDate = new Date();
        const endDate = new Date();
        endDate.setDate(addDate.getDate() + 1);
        let story = new storyModel({ ...body });
        story.date = addDate;
        story.endDate = endDate;
        story.owner = owner;
        story.likesNum = 0;
        await story.save();
        return story;
        // const addDate = new Date();
        // const endDate = new Date();
        // endDate.setDate(addDate.getDate() + 1);
        // await userModel.updateOne({ _id: owner }, { $push: { stories: { ...body, date: addDate, endDate: endDate, likesNum: 0 } } });
        // let user = await userModel.find({ _id: owner });
        // return user[0].stories[user[0].stories.length - 1];
    }

    static async addLike(storyId, userId) {
        let story = await storyModel.findById(storyId);
        story.likes.push({ user: userId, date: new Date });
        story.likesNum = story.likesNum + 1;
        await story.save();
        notificationController.addStoryLikeNotification(story.owner, storyId, userId)
            .then((_) => { })
            .catch((e) => {
                errorController.saveError("story.controller => add like notification", e.message);
            });
        return story;
    }

    static async removeLike(storyId, userId) {
        let story = await storyModel.findById(storyId);
        let tmpLikes = [...story.likes];
        story.likes = story.likes.filter((like) => like.user.toString() !== userId.toString());
        story.likesNum = story.likesNum - 1;
        await story.save();
        if (tmpLikes.length != story.likes.length) {
            notificationController.removeStoryLikeNotification(story.owner, storyId, userId)
                .then((_) => { })
                .catch((e) => {
                    errorController.saveError("story.controller => remove like notification", e.message);
                });
        }
        return story.likes;
    }

    static async getLikes(storyId, page, userId) {
        let story = await storyModel.findOne({ _id: storyId }, { likes: { $slice: [(page - 1) * 30, 30] } }).populate('likes.user', { image: 1, name: 1, followers: 1 });
        const user = await userModel.findById(userId);
        const blockedIds = blockController.getBLockIdsFromUserObject(user);
        story.likes = story.likes.filter((like) => !blockedIds.includes(like.user._id.toString()));
        story.likes.forEach((like, i) => {
            story.likes[i] = story.likes[i].toObject();
            story.likes[i].isFollowing = like.user.followers.findIndex(follow => follow.user.toString() == userId.toString()) == -1 ? false : true;
            delete story.likes[i].user.followers;
        });
        return story.likes;
    }

    static async getMainStories(userId, page) {
        const user = await userModel.findById(userId).select({ following: 1, _id: 0 });
        let followingIDs = getIDs(user.following);
        const blockedIds = blockController.getBLockIdsFromUserObject(user);
        let topStories = await storyModel.find({ owner: { $nin: blockedIds } }).sort({ likesNum: -1 }).skip(0).limit(5).populate('owner', { _id: 1, image: 1, name: 1 });
        let stories = await storyModel.find({ owner: { $in: followingIDs } }).sort({ date: -1 }).skip((page - 1) * 10).limit(10).populate('owner', { _id: 1, image: 1, name: 1 });
        let newStories = stories.map((story) => {
            story = story.toObject();
            story.likesNum = story.likes.length
            story.iLike = story.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            story.isFollowing = (user.following.findIndex((follow) => follow.user.toString() === story.owner._id.toString())) == -1 ? false : true;
            delete story.likes;
            return story;
        });
        return topStories.concat(newStories);
    }

    static async getMainAggregatedStories(userId, page) {
        const cnt = 10, offset = (page - 1) * cnt;
        const user = await userModel.findById(userId).select({ following: 1, _id: 0 });
        let followingIDs = getIDsAsObjectID(user.following);
        const blockedIds = blockController.getBLockIdsFromUserObject(user);
        //let topStories = await storyModel.find({ owner: { $nin: blockedIds } }).sort({ likesNum: -1 }).skip(0).limit(5).populate('owner', { _id: 1, image: 1, name: 1 });
        let topStories = await storyModel.aggregate([
            { $match: { owner: { $nin: followingIDs } } },
            { $lookup: { from: 'users', localField: 'owner', foreignField: '_id', as: 'owner' } },
            { $sort: { 'endDate': -1 } },
            {
                $group: {
                    _id: "$owner._id",
                    name: { $first: "$owner.name" },
                    image: { $first: "$owner.image" },
                    stories: {
                        $push: {
                            verses: '$verses',
                            name: '$name',
                            caption: '$caption',
                            date: '$date',
                            likesNum: '$likesNum',
                            backgroundImage: '$backgroundImage',
                            _id: '$_id',
                            endDate: '$endDate',
                            iLike: {
                                '$cond': [{
                                    '$gt': [{
                                        $size: {
                                            '$filter': {
                                                'input': '$likes',
                                                'as': 'like',
                                                'cond': {
                                                    '$eq': [
                                                        { $toString: '$$like.user' },
                                                        userId.toString()
                                                    ]
                                                }
                                            }
                                        }
                                    }
                                        , 0]
                                }, true, false]
                            }
                        }
                    },
                },
            },

            { $sort: { 'stories.endDate': -1 } },
            { $limit: 5 },
        ]);
        let userStories = await storyModel.aggregate([
            { $match: { owner: { $in: followingIDs } } },
            { $lookup: { from: 'users', localField: 'owner', foreignField: '_id', as: 'owner' } },
            { $sort: { 'endDate': -1 } },
            {
                $group: {
                    _id: "$owner._id",
                    name: { $first: "$owner.name" },
                    image: { $first: "$owner.image" },
                    stories: {
                        $push: {
                            verses: '$verses',
                            name: '$name',
                            caption: '$caption',
                            date: '$date',
                            likesNum: '$likesNum',
                            backgroundImage: '$backgroundImage',
                            _id: '$_id',
                            endDate: '$endDate',
                            iLike: {
                                '$cond': [{
                                    '$gt': [{
                                        $size: {
                                            '$filter': {
                                                'input': '$likes',
                                                'as': 'like',
                                                'cond': {
                                                    '$eq': [
                                                        { $toString: '$$like.user' },
                                                        userId.toString()
                                                    ]
                                                }
                                            }
                                        }
                                    }
                                        , 0]
                                }, true, false]
                            }
                        }
                    },
                },
            },

            { $sort: { 'stories.endDate': -1 } },
            { $skip: offset },
            { $limit: cnt },
        ]);
        return topStories.concat(userStories);
    }
}