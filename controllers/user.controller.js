const userModel = require('../models/user.model');
const poemModel = require('../models/poem.model');
const JWT = require('jsonwebtoken');
const userUtil = require('../utils/users.util');
const bcrypt = require('bcrypt');
const randomString = require('randomstring');
const mailer = require('./mailer.controller');

module.exports = class User {
    static async logUser(req, res) {
        if (req.body.faceToken) {
            return User.logFBUser(req, res);
        }
        else if (req.body.googleToken) {
            return User.logGoogleUser(req, res);
        }
        else if (req.body.appleToken) {
            return User.logAppleUser(req, res);
        }
        else {
            return res.status(401).json({});
        }
    }

    static async logFBUser(req, res) {
        try {
            const isFBUserOrID = await userUtil.isFBUser(req.body.faceId, req.body.faceToken);
            if (isFBUserOrID) {
                let foundUsers;
                try {
                    foundUsers = await userModel.find({ faceId: req.body.faceId });
                } catch (e) {
                    res.status(500).json({ error: e, message: e.message });
                }
                if (foundUsers.length > 0) {
                    return User._loginFB(req, res);
                }

                const user = userModel();
                user.faceId = isFBUserOrID;
                user.faceToken = req.body.faceToken;
                await user.save();
                const token = await user.generateAuthToken();
                return res.status(201).json({
                    user,
                    token
                });
            } else {
                return res.status(401).json({ message: "unauthorized" });
            }

        } catch (e) {
            res.status(500).json(e.message);
        }
    }


    static async _loginFB(req, res) {
        const user = await userModel.findOne({ faceId: req.body.faceId });
        const token = await user.generateAuthToken();
        return res.status(200).send({
            user,
            token
        });
    }

    static async logGoogleUser(req, res) {
        try {
            const isGoogleUserOrID = await userUtil.isGoogleUser(req.body.googleId, req.body.googleToken);
            if (isGoogleUserOrID) {
                let foundUsers;
                try {
                    foundUsers = await userModel.find({ googleId: req.body.googleId });
                } catch (e) {
                    res.status(500).json({ error: e, message: e.message });
                }
                if (foundUsers.length > 0) {
                    return User._loginGoogle(req, res);
                }

                const user = userModel();
                user.googleId = req.body.googleId;
                user.googleToken = req.body.googleToken;
                await user.save();
                const token = await user.generateAuthToken();
                return res.status(201).json({
                    user,
                    token
                });
            } else {
                return res.status(401).json({ message: "unauthorized" });
            }

        } catch (e) {
            res.status(500).json(e.message);
        }
    }

    static async _loginGoogle(req, res) {
        const user = await userModel.findOne({ googleId: req.body.googleId });
        const token = await user.generateAuthToken();
        return res.status(200).send({
            user,
            token
        });
    }

    static async logAppleUser(req, res) {
        try {
            const isAppleUserOrID = req.body.appleId;
            if (isAppleUserOrID) {
                let foundUsers;
                try {
                    foundUsers = await userModel.find({ appleId: req.body.appleId });
                } catch (e) {
                    res.status(500).json({ error: e, message: e.message });
                }
                if (foundUsers.length > 0) {
                    return User._loginApple(req, res);
                }

                const user = userModel();
                user.appleId = isAppleUserOrID;
                user.appleToken = req.body.appleToken;
                await user.save();
                const token = await user.generateAuthToken();
                return res.status(201).json({
                    user,
                    token
                });
            } else {
                return res.status(401).json({ message: "unauthorized" });
            }

        } catch (e) {
            res.status(500).json(e.message);
        }

    }
    static async _loginApple(req, res) {
        try {
            const user = await userModel.findOne({ appleId: req.body.appleId });
            const token = await user.generateAuthToken();
            return res.status(200).send({
                user,
                token
            });
        } catch (e) {
            console.log(e);
            res.status(403).json(e.message);
        }
    }

    static async refreshJWTToken(token) {
        try {
            JWT.verify(token, process.env.JWT_KEY);
            throw new Error("old token is active");
        } catch (e) {
            if (e instanceof JWT.TokenExpiredError) {
                let user = await userModel.findOne({ 'tokens.token': token });
                if (user != null) {
                    const newToken = await user.generateAuthToken();
                    return newToken;
                }
            }
            throw new Error(e.message);
        }
    }

    static async getUserProfile(userID) {
        let user = await userModel.findOne({ _id: userID }, { name: 1, image: 1, bio: 1, birthdate: 1, city: 1, country: 1, language: 1, otherAdress: 1, category: 1, followers: 1, following: 1 });
        user = user.toObject();
        user.poemsNum = await poemModel.find({ owner: userID }).countDocuments();
        user.followersNum = user.followers.length;
        user.followingNum = user.following.length;
        delete user.followers;
        delete user.following;
        return user;
    }

    static async edit(user_id, fcmToken, image, name, language, bio, isPoet, birthdate, country, city, otherAdress, category) {
        let user = await userModel.findById(user_id);
        user.fcmToken = fcmToken;
        user.image = image;
        user.name = name;
        user.language = language;
        user.bio = bio;
        user.isPoet = isPoet;
        user.birthdate = birthdate;
        user.country = country;
        user.city = city;
        user.otherAdress = otherAdress;
        user.category = category;
        await user.save();
        return user;

    }

    static async signup(email, password) {
        const hashedPassword = await bcrypt.hash(password, 2);
        let user = await userModel.findOne({ email: email });
        const verifyCode = randomString.generate({ charset: "0123456789", length: 6 });
        if (user) {
            throw new Error('Email already in use!')
        } else {
            let newUser = new userModel({ email: email, password: hashedPassword, verified: false, verificationCode: verifyCode });
            await newUser.save();
            newUser = newUser.toObject();
            mailer.sentVerificationMail(newUser.email, newUser.verificationCode);
            delete newUser.verificationCode;
            return newUser;
        }
    }

    static async login(email, password) {
        let user = await userModel.findOne({ email: email });
        if (user) {
            let isCorrect = await bcrypt.compare(password, user.password);
            if (isCorrect) {
                if (user.verified) {
                    const token = await user.generateAuthToken();
                    user = user.toObject();
                    delete user.password;
                    delete user.verified;
                    return {
                        user,
                        token
                    };
                } else {
                    return "NOTVERIFIED";
                }
            } else {
                //password is not correct
                throw new Error("Email and/or password is not correct");
            }
        } else {
            //email is not correct
            throw new Error("Email and/or password is not correct");
        }
    }

    static async verifyEmail(email, code) {
        let result = await userModel.updateOne({ email: email, verificationCode: code }, { verified: true });
        return result.n;
    }

    static async sentVerificationCode(email) {
        let user = await userModel.findOne({ email: email });
        user.verificationCode = randomString.generate({ charset: '0123456789', length: 6 });
        await user.save();
        mailer.sentVerificationMail(user.email, user.verificationCode);
        return "sent done!";
    }

    static async resetPassword(email, code, newPassword) {
        let hashedPassword = bcrypt.hashSync(newPassword, 2);
        let result = await userModel.updateOne({ email: email, verificationCode: code }, { password: hashedPassword });
        if (result.n == 1) {
            return "updated password";
        }
        throw new Error("Failed to update password, check your email address and/or verification code");
    }

}