const userModel = require('../models/user.model');
const followController = require('./following.controller');

module.exports = class Block {
    static async add(userId, blockUserId) {

        const blockDate = new Date();
        // add blockd user to user 1
        let user = await userModel.findById(userId);
        user = await followController.removeFollowingAndFollowerFromUserObject(user, blockUserId);
        user.blocking.push({ user: blockUserId, date: blockDate });
        await user.save();

        // add blocker to
        let user2 = await userModel.findById(blockUserId);
        user2 = await followController.removeFollowingAndFollowerFromUserObject(user2, userId);
        user2.blockers.push({ user: userId, date: blockDate });
        await user2.save();
    }

    static async remove(userId, user2Id) {
        let user = await userModel.findById(userId);
        user.blocking = user.blocking.filter((block) => block.user.toString() !== user2Id.toString());
        await user.save();

        let user2 = await userModel.findById(user2Id);
        user2.blockers = user2.blockers.filter((block) => block.user.toString() !== userId.toString());
        await user2.save();
    }

    static async getBlockingUsers(userId, page) {
        let user = await userModel.findOne({ _id: userId }, { blocking: { $slice: [(page - 1) * 30, 30] } }).populate('blocking.user', { id: 1, name: 1, image: 1 });
        return user.blocking;
    }

    static getBLockIdsFromUserObject(user) {
        let ids = [];
        if (user.blocking) {
            user.blocking.forEach((block) => {
                ids.push(block.user.toString());
            });
        }
        if (user.blockers) {
            user.blockers.forEach((block) => {
                ids.push(block.user.toString());
            });
        }

       
        let set = new Set(ids);
        return [...set];
    }
}