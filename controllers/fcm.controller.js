const fcm = require('node-gcm');
const appKey = "firebase key";
const sender = fcm.Sender(appKey);
const errorController = require('./error.controller');

module.exports = class FCMNotification {
    static async sentFollowNotification(language, userId, username, token) {
        let message;
        if (language == 'ar') {
            message = fcm.Message({
                notification: {
                    title: "لديك متابع جديد",
                    body: `قام ${username} بمتابعتك`
                },
                body: {
                    click_action: "FLUTTER_NOTIFICATION_CLICK",
                    userId: userId
                }
            });
        }
        else {
            message = fcm.Message({
                notification: {
                    title: "You have new follower",
                    body: `${username} is following you`
                },
                body: {
                    click_action: "FLUTTER_NOTIFICATION_CLICK",
                    userId: userId
                }
            })
        }
        FCMNotification._sentMessage(sender, [token], message);
    }

    static _sentMessage(sender, tokens, message) {
        sender.send(message, tokens == null ? null : tokens, (error, response) => {
            if (error) {
                console.log(error);
                errorController.saveError("fcm.controller => _sendMessage", e.message);
            }
            else {
                console.log(response);
            }
        });
    }
}