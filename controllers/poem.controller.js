const poemModel = require('../models/poem.model');
const userModel = require('../models/user.model');
const blockController = require('./block.controller');
const notificationController = require('./notification.controller');
const errorController = require('./error.controller');
const hashController = require('./hashtag.controller');
const getIDs = require('../utils/getIDsFromFollowing');

module.exports = class Poem {
    static async add(body, owner) {
        let poem = new poemModel({ ...body });
        poem.date = new Date();
        poem.owner = owner;
        poem.likesNum = 0;
        poem.viewsCount = 0;
        poem.favoriteBy = [];
        await poem.save();
        hashController.upgradeHashtags(poem.hashtags).catch((e) => {
            errorController.saveError("shatags.controller => upgrade", e.message);
        });
        return poem;
    }

    static async addLike(poemId, userId) {
        //handle if user is like this post or not ??????!!!!!!!!!!!!!!!!!!
        let poem = await poemModel.findById(poemId);
        poem.likes.push({ user: userId, date: new Date });
        poem.likesNum = poem.likesNum + 1;
        poem.viewsCount = 0;
        await poem.save();
        notificationController.addPoemLikeNotification(poem.owner, poemId, userId).then((_) => { }).catch((e) => {
            errorController.saveError("poem.controller.js => addLikeNptifiaction", e.message);
        });
        return poem;
    }

    static async removeLike(poemId, userId) {
        let poem = await poemModel.findById(poemId);
        let tmpLikes = [...poem.likes];
        poem.likes = poem.likes.filter((like) => like.user.toString() !== userId.toString());
        poem.likesNum = poem.likesNum - 1;
        await poem.save();
        if (tmpLikes.length != poem.likes.length) {
            notificationController.removePoemLikeNotification(poem.owner, poemId, userId)
                .then((_) => { })
                .catch((e) => {
                    errorController.saveError("poem.controller => remove like notification", e.message);
                });
        }
        return poem.likes;
    }

    static async getLikes(poemId, page, userId) {
        let poem = await poemModel.findOne({ _id: poemId }, { likes: { $slice: [(page - 1) * 30, 30] } }).populate('likes.user', { image: 1, name: 1, followers: 1 });
        const user = await userModel.findById(userId);
        const blockedIds = blockController.getBLockIdsFromUserObject(user);
        poem.likes = poem.likes.filter((like) => !blockedIds.includes(like.user._id.toString()));
        poem.likes.forEach((like, i) => {
            poem.likes[i] = poem.likes[i].toObject();
            poem.likes[i].isFollowing = like.user.followers.findIndex(follow => follow.user.toString() == userId.toString()) == -1 ? false : true;
            delete poem.likes[i].user.followers;
        });
        return poem.likes;
    }

    static async getMainPosts(userId, page) {
        const user = await userModel.findById(userId).select({ following: 1, _id: 0 });
        let followingIDs = getIDs(user.following);
        let poems = await poemModel.find({ owner: { $in: followingIDs } }).sort({ date: -1 }).skip((page - 1) * 10).limit(10).populate('owner', { _id: 1, image: 1, name: 1 }).populate('storeElement', { image: 1 });
        let newPoems = poems.map((poem) => {
            poem = poem.toObject();
            poem.likesNum = poem.likes.length
            poem.iLike = poem.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            poem.isFollowing = (user.following.findIndex((follow) => follow.user.toString() === poem.owner._id.toString())) == -1 ? false : true;
            poem.favoriteBy = poem.favoriteBy.findIndex((fav) => fav.user.toString() === userId.toString()) == -1 ? false : true;
            delete poem.likes;
            return poem;
        });
        return newPoems;
    }

    static async addToFavorites(poemId, userId) {
        let result = await poemModel.updateOne({ _id: poemId }, { $addToSet: { favoriteBy: { user: userId, date: new Date() } } });
        return result;

    }

    static async deleteToFavorites(poemId, userId) {
        let result = await poemModel.updateOne({ _id: poemId }, { $pull: { favoriteBy: { user: userId } } });
        return result;

    }

    static async getFavoritePoems(userId, page) {
        let cnt = 10, offset = (page - 1) * cnt;
        let poems = await poemModel.find({ "favoriteBy.user": userId }, { name: 1, caption: 1, date: 1, likes: 1, viewsCount: 1, backgroundColor: 1, backgroundImage: 1, verses: 1, versesType: 1, owner: 1, "favoriteBy.user.$.user": userId }).sort({ "favoriteBy.date": -1 }).populate('owner', { name: 1, image: 1 });
        let returnPoems = [];
        poems.forEach((poem) => {
            poem = poem.toObject();
            poem.favoriteBy = poem.favoriteBy.findIndex((fav) => fav.user.toString() === userId.toString()) == -1 ? false : true;
            poem.iLike = poem.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            poem.likesNum = poem.likes.length;
            delete poem.likes;
            returnPoems.push(poem);
        });
        return returnPoems;
    }

    static async getMyPoems(page, userId) {
        let cnt = 10, offset = (page - 1) * cnt;
        let poems = await poemModel.find({ owner: userId }).sort({ date: -1 }).skip(offset).limit(cnt);
        let returnPoems = [];
        poems.forEach((poem) => {
            poem = poem.toObject();
            poem.favoriteBy = poem.favoriteBy.findIndex((fav) => fav.user.toString() === userId.toString()) == -1 ? false : true;
            poem.iLike = poem.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            delete poem.likes;
            returnPoems.push(poem);
        });
        return returnPoems;
    }

    static async incViewsCount(poemId) {
        await poemModel.updateOne({ _id: poemId }, { $inc: { viewsCount: 1 } });
        return true;
    }

    static async getTodayPoem(userId) {
        const nowDate = new Date();
        const dateToCompare = new Date();
        dateToCompare.setDate(nowDate.getDate() - 30);
        let poems = await poemModel.aggregate([
            { $lookup: { from: 'users', localField: 'owner', foreignField: '_id', as: 'owner' } },
            {
                $project: {
                    name: 1,
                    caption: 1,
                    date: 1,
                    backgroundImage: 1,
                    backgroundColor: 1,
                    verses: 1,
                    versesType: 1,
                    viewsCount: 1,
                    owner: { _id: 'owner._id', image: 'owner.image', name: 'owner.name' },
                    iLike: {
                        '$cond': [{
                            '$gt': [{
                                $size: {
                                    '$filter': {
                                        'input': '$likes',
                                        'as': 'like',
                                        'cond': {
                                            '$eq': [
                                                { $toString: '$$like.user' },
                                                userId.toString()
                                            ]
                                        }
                                    }
                                }
                            }
                                , 0]
                        }, true, false]
                    },
                    favoriteBy: {
                        '$cond': [{
                            '$gt': [{
                                $size: {
                                    '$filter': {
                                        'input': '$favoriteBy',
                                        'as': 'fav',
                                        'cond': {
                                            '$eq': [
                                                { $toString: '$$fav.user' },
                                                userId.toString()
                                            ]
                                        }
                                    }
                                }
                            }
                                , 0]
                        }, true, false]
                    },
                    likesCount: {
                        $size: {
                            $filter: {
                                input: '$likes',
                                as: 'like',
                                cond: {
                                    $gt: ['$$like.date', dateToCompare]
                                }
                            }
                        }
                    }
                }
            },
            { $sort: { likesCount: -1 } },
            { $limit: 5 }
        ]);

        return poems;
    }

    static async delete(userId, poemId) {
        let result = await poemModel.deleteOne({ _id: poemId, owner: userId });
        return result;
    }

    static async edit(userId, poemId, name, hashtags, caption, verses, backgroundImage, backgroundColor, type) {
        let poem = await poemModel.findOne({ _id: poemId, owner: userId });
        if (poem != null) {
            poem.hashtags = hashtags;
            poem.name = name;
            poem.versesType = type;
            poem.caption = caption;
            poem.verses = verses;
            poem.backgroundColor = backgroundColor;
            poem.backgroundImage = backgroundImage;
            await poem.save();
            return poem;
        } else {
            throw new Error("This Operation Is Not Available!");
        }
    }
}