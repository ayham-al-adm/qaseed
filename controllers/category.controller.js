const categoryModel = require('../models/category.model');

module.exports = class Category {
    static async addNewCategory(data) {
        let category = new categoryModel({ ...data });
        await category.save();
        return category;
    }

    static async getUserCategories() {
        let cats = await categoryModel.find({ type: 'USER' });
        return cats;
    }

    static async getPostCategories() {
        let cats = await categoryModel.find({ type: 'POST' });
        return cats;
    }
}