const notificationModel = require('../models/notification.model');
const mongoose = require('mongoose');

module.exports = class Notification {
    static async addPoemLikeNotification(ownerId, poemId, userId) {
        const likeDate = new Date();
        await notificationModel.updateOne({ owner: ownerId, n_type: 'P_LIKE', poem: poemId }, { is_new: true, nDate: likeDate, $push: { users: { user: userId, date: likeDate } } }, { upsert: true });
        return true;
    }

    static async removePoemLikeNotification(ownerId, poemId, userId) {
        await notificationModel.updateOne({ owner: ownerId, n_type: 'P_LIKE', poem: poemId }, { $pull: { users: { user: userId } } }, { multi: true });
        return true;
    }


    static async addStoryLikeNotification(ownerId, storyId, userId) {
        const likeDate = new Date();
        await notificationModel.updateOne({ owner: ownerId, n_type: 'S_LIKE', story: storyId }, { is_new: true, nDate: likeDate, $push: { users: { user: userId, date: likeDate } } }, { upsert: true });
        return true;
    }

    static async removeStoryLikeNotification(ownerId, storyId, userId) {
        await notificationModel.updateOne({ owner: ownerId, n_type: 'S_LIKE', story: storyId }, { $pull: { users: { user: userId } } }, { multi: true });
        return true;
    }

    static async addFollowNotification(ownerId, userId) {
        const likeDate = new Date();
        await notificationModel.updateOne({ owner: ownerId, n_type: 'FOLLOW' }, { is_new: true, nDate: likeDate, $push: { users: { user: userId, date: likeDate } } }, { upsert: true });
        return true;
    }

    static async removeFollowNotification(ownerId, userId) {
        await notificationModel.updateOne({ owner: ownerId, n_type: 'FOLLOW' }, { $pull: { users: { user: userId } } });
        return true;
    }

    static async addRejectPoemNotification(ownerId, poemId) {
        const likeDate = new Date();
        await notificationModel.updateOne({ owner: ownerId, n_type: 'P_REJECT', poem: poemId }, { is_new: true, nDate: likeDate }, { upsert: true });
        return true;
    }

    static async addRejectStoryNotification(ownerId, storyId) {
        const likeDate = new Date();
        await notificationModel.updateOne({ owner: ownerId, n_type: 'S_REJECT', story: storyId }, { is_new: true, nDate: likeDate }, { upsert: true });
        return true;
    }

    static async getUserNotifications(userId, page) {
        let cnt = 20, offset = (page - 1) * cnt;
        let nots = await notificationModel.find({ owner: userId, "users.0": { $exists: true } }).sort({ is_new: -1, nDate: -1 }).skip(offset).limit(cnt).populate("users.user", { image: 1, name: 1 });
        let returnNots = nots.map((not) => {
            not = not.toObject();
            not.othersCount = not.users.length - 1;
            not.lastUser = not.users[not.othersCount].user;
            delete not.users;
            return not;
        });
        return returnNots;
    }

    static async getNewNotificationsCount(userID) {
        return notificationModel.find({ owner: userID, is_new: true, "users.0": { $exists: true } }).count();

    }

    static async markAllAsReaded(userID) {
        return notificationModel.updateMany({ owner: userID, is_new: true, "users.0": { $exists: true } }, { is_new: false });
    }
}