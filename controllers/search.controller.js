const userModel = require('../models/user.model');
const poemsModel = require('../models/poem.model');
const blockController = require('./block.controller');

const getIDsFromFollowing = require('../utils/getIDsFromFollowing');

module.exports = class Search {
    static async searchUsers(keyword, page, userId) {
        let cnt = 10, offset = (page - 1) * cnt;
        let user = await userModel.findById(userId);
        let blockedIDs = blockController.getBLockIdsFromUserObject(user);
        let users = await userModel.find({ $and: [{ _id: { $nin: blockedIDs } }, { name: { $regex: keyword } }] }, { image: 1, name: 1, cover: 1 }).skip(offset).limit(cnt);

        let folloIds = getIDsFromFollowing(user.following);
        let returnUsers = users.map((tUser) => {
            tUser = tUser.toObject();
            tUser.isFollowing = folloIds.includes(tUser._id.toString());
            return tUser;
        });

        return returnUsers;
    }

    static async searchUsersByCategory(keyword, page, userId, categoryId) {
        let cnt = 10, offset = (page - 1) * cnt;
        let user = await userModel.findById(userId);
        let blockedIDs = blockController.getBLockIdsFromUserObject(user);
        let users = await userModel.find({ $and: [{ _id: { $nin: blockedIDs } }, { name: { $regex: keyword } }, { category: categoryId }] }, { image: 1, name: 1, cover: 1 }).skip(offset).limit(cnt);

        let folloIds = getIDsFromFollowing(user.following);
        let returnUsers = users.map((tUser) => {
            tUser = tUser.toObject();
            tUser.isFollowing = folloIds.includes(tUser._id.toString());
            return tUser;
        });

        return returnUsers;
    }

    static async searchPoems(keyword, page, userId) {
        let cnt = 10, offset = (page - 1) * cnt;
        let user = await userModel.findById(userId);
        let blockedIDs = blockController.getBLockIdsFromUserObject(user);

        let folloIds = getIDsFromFollowing(user.following);

        let poems = await poemsModel.find({ $and: [{ owner: { $nin: blockedIDs } }, { $or: [{ "verses.verse1": { $regex: keyword } }, { "verses.verse2": { $regex: keyword } }] }] }, { hashtags: 0, likes: 0 }).skip(offset).limit(cnt).populate('owner', { name: 1, image: 1 });
        let returnPoems = poems.map((poem) => {
            poem = poem.toObject();
            poem.isFollowing = folloIds.includes(poem.owner._id.toString());
            return poem;
        });
        return returnPoems;
    }

    static async searchPoemsByCategory(keyword, page, userId, categoryId) {
        let cnt = 10, offset = (page - 1) * cnt;
        let user = await userModel.findById(userId);
        let blockedIDs = blockController.getBLockIdsFromUserObject(user);

        let folloIds = getIDsFromFollowing(user.following);

        let poems = await poemsModel.find({ $and: [{ owner: { $nin: blockedIDs } }, { $or: [{ "verses.verse1": { $regex: keyword } }, { "verses.verse2": { $regex: keyword } }] }, { category: categoryId }] }, { hashtags: 0 }).skip(offset).limit(cnt).populate('owner', { name: 1, image: 1 });
        let returnPoems = poems.map((poem) => {
            poem = poem.toObject();
            poem.isFollowing = folloIds.includes(poem.owner._id.toString());
            poem.favoriteBy = poem.favoriteBy.findIndex((fav) => fav.user.toString() === userId.toString()) == -1 ? false : true;
            poem.iLike = poem.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            delete poem.likes;
            return poem;
        });
        return returnPoems;
    }

    static async searchMyPoems(keyword, page, userId) {
        let cnt = 10, offset = (page - 1) * cnt;

        let poems = await poemsModel.find({ $and: [{ owner: userId }, { $or: [{ "verses.verse1": { $regex: keyword } }, { "verses.verse2": { $regex: keyword } }] }] }, { hashtags: 0 }).skip(offset).limit(cnt).populate('owner', { name: 1, image: 1 });
        let returnPoems = poems.map((poem) => {
            poem = poem.toObject();
            poem.favoriteBy = poem.favoriteBy.findIndex((fav) => fav.user.toString() === userId.toString()) == -1 ? false : true;
            poem.iLike = poem.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            delete poem.likes;
            return poem;
        });
        return returnPoems;
    }

}