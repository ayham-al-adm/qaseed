const userModel = require('../models/user.model');
const poemsModel = require('../models/poem.model');
const blockController = require('./block.controller');

module.exports = class Other {
    static async getOtherProfile(otherId, userId) {
        let user = await userModel.findOne({ _id: otherId }/*, { name: 1, image: 1, isPoet: 1, birthdate: 1, country: 1, city: 1, otherAdress: 1, poemsNum: 1, followingNum: 1, followersNum: 1, cover: 1, followers: 1, blocking: 1 }*/).populate('category', { name: 1, image: 1 });
        user = user.toObject();
        user.isFollowing = user.followers.findIndex((follow) => follow.user.toString() === userId.toString());
        user.iBlocking = user.blocking.findIndex((block) => block.user.toString() === userId.toString()) == -1 ? false : true;
        user.iBlocker = user.blockers.findIndex((block) => block.user.toString() === userId.toString()) == -1 ? false : true;
        user.followersCount = user.followers.length;
        user.followingCount = user.following.length;
        user.poemsCount = await poemsModel.find({ owner: otherId }).countDocuments();
        delete user.followers;
        delete user.blocking;
        delete user.blockers;
        return user;
    }

    static async getOtherPoems(page, otherId, userId) {
        let poems = await poemsModel.find({ owner: otherId }).populate('owner', { image: 1, name: 1 }).skip((page - 1) * 10).limit(10);
        let returnPoems = [];
        poems.forEach((poem) => {
            poem = poem.toObject();
            poem.iLike = poem.likes.findIndex((like) => like.user.toString() === userId.toString()) == -1 ? false : true;
            poem.favoriteBy = poem.favoriteBy.findIndex((fav) => fav.user.toString() === userId.toString()) == -1 ? false : true;
            delete poem.likes;
            returnPoems.push(poem);
        });
        return returnPoems;
    }

    static async getOtherFollowers(page, otherId, userId) {
        let me = await userModel.findById(userId);
        let blockedIDs = blockController.getBLockIdsFromUserObject(me);
        let user = await userModel.findOne({ _id: otherId }, { followers: { $slice: [(page - 1) * 30, 30] } }).populate('followers.user', { _id: 1, image: 1, name: 1, followers: 1 });
        let res = user.followers;
        let returnUsers = res.map((follow, index) => {
            if (!blockedIDs.includes(follow.user)) {
                let findIndex = follow.user.followers.findIndex((element) => element.user.toString() === userId.toString());
                res[index] = res[index].toObject();
                res[index].isFollow = findIndex;
                delete res[index].user.followers;
                delete res[index].date;
                delete res[index]._id;
                return res[index];
            }
        });
        return returnUsers;
    }

    static async getOtherFollowing(page, otherId, userId) {
        let me = await userModel.findById(userId);
        let blockedIDs = blockController.getBLockIdsFromUserObject(me);
        let user = await userModel.findOne({ _id: otherId }, { following: { $slice: [(page - 1) * 30, 30] } }).populate('followers.user', { _id: 1, image: 1, name: 1, followers: 1 });
        let res = user.following;
        let returnUsers = res.map((follow, index) => {
            if (!blockedIDs.includes(follow.user)) {
                let findIndex = follow.user.followers.findIndex((element) => element.user.toString() === userId.toString());
                res[index] = res[index].toObject();
                res[index].isFollow = findIndex;
                delete res[index].user.followers;
                delete res[index].date;
                delete res[index]._id;
                return res[index];
            }
        });
        return returnUsers;
    }
}