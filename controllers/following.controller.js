const userModel = require('../models/user.model');
const fcmNotification = require('./fcm.controller');
const notificationController = require('../controllers/notification.controller');
const errorController = require('./error.controller');
const errorModel = require('../models/error.model');
const getFollowIds = require('../utils/getIDsFromFollowing');
const { getBLockIdsFromUserObject } = require('./block.controller');


module.exports = class Following {

    static async addFollow(fromUserId, toUserId) {
        if (fromUserId.toString() === toUserId.toString())
            return false;
        const followDate = new Date().getTime();
        let user = await userModel.findById(fromUserId);
        let index = user.following.findIndex((element) => element.user.toString() === toUserId.toString());
        if (index == -1) {
            user.following.push({ user: toUserId, date: followDate });
            await user.save();
        }

        let user2 = await userModel.findById(toUserId);
        index = user2.followers.findIndex((element) => element.user.toString() === fromUserId.toString());
        if (index == -1) {
            user2.followers.push({ user: fromUserId, date: followDate });
            await user2.save();
            fcmNotification.sentFollowNotification(user2.language, user._id, user.name, user2.fcmToken)
                .catch((e) => {
                    errorModel.saveError("following controller => fcm notification", e.message);
                })
            notificationController.addFollowNotification(user2._id, user._id)
                .then((_) => { })
                .catch((e) => {
                    errorController.saveError("following.controller => add follow notfication", e.message);
                });
        }

        return true;
    }

    static async removeFollow(fromUserId, toUserId) {

        let user = await userModel.findById(fromUserId);
        let index = user.following.findIndex((element) => element.user == toUserId);
        if (index != -1) {
            user.following.splice(index, 1);
            await user.save();
        }

        let user2 = await userModel.findById(toUserId);
        index = user2.followers.findIndex((element) => element.user == fromUserId);
        if (index != -1) {
            user2.followers.splice(index, 1);
            await user2.save();
            notificationController.removeFollowNotification(user2._id, fromUserId);
        }

        return true;
    }

    static async removeFollowingAndFollowerFromUserObject(user, toRemoveID) {
        let index = user.following.findIndex((element) => element.user == toRemoveID);
        if (index != -1) {
            user.following.splice(index, 1);
        }
        index = user.followers.findIndex((element) => element.user == toRemoveID);
        if (index != -1) {
            user.followers.splice(index, 1);
            await user.save();
            //notificationController.removeFollowNotification(user._id);
        }

        return user;
    }


    static async getFollowing(userId, page) {
        let user = await userModel.findOne({ _id: userId }, { following: { $slice: [(page - 1) * 30, 30] } }).populate('following.user', { image: 1, name: 1, _id: 1 });
        return user.following;
    }

    static async getFollowers(userId, page) {
        let user = await userModel.findOne({ _id: userId }, { followers: { $slice: [(page - 1) * 30, 30] } }).populate('followers.user', { _id: 1, image: 1, name: 1, followers: 1 });
        let res = user.followers;
        res.forEach((follow, index) => {
            let findIndex = follow.user.followers.findIndex((element) => { console.log(element); return element.user == userId; });
            res[index] = res[index].toObject();
            res[index].isFollow = findIndex;
            delete res[index].user.followers;
        });
        return res;
    }

    static async removeFollowingAndFollowerFromUserObject(user, toRemoveID) {
        let index = user.following.findIndex((element) => element.user == toRemoveID);
        if (index != -1) {
            user.following.splice(index, 1);
        }
        index = user.followers.findIndex((element) => element.user == toRemoveID);
        if (index != -1) {
            user.followers.splice(index, 1);
            await user.save();
            notificationController.removeFollowNotification(user._id);
        }

        return user;
    }

    static async getSuggestedUsers(userId, page) {
        const cnt = 20, offset = (page - 1) * cnt;
        let user = await userModel.findById(userId);
        let followingIds = getFollowIds(user.following);
        let blockedIds = getBLockIdsFromUserObject(user);
        let users = await userModel.find({ _id: { $nin: followingIds.concat(blockedIds).concat(userId) } }, {image: 1, name: 1}).sort({ 'followingNum': -1 }).skip(offset).limit(cnt);
        return users;
    }
}