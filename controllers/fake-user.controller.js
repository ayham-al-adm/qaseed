const poemModel = require('../models/poem.model');
const userModel = require('../models/user.model');

const hashController = require('./hashtag.controller');
const errorController = require('./error.controller');

module.exports = class FakeUser {

    static async getAllFakeUsers() {
        let users = await userModel.find({ isFake: true });
        return users;
    }

    static async addFakeUser(data) {
        let user = new userModel({ ...data });
        user.isFake = true;
        await user.save();
        return user;
    }


    static async addPoem(data) {
        let poem = new poemModel({ ...data });
        poem.date = new Date();
        poem.likesNum = 0;
        await poem.save();
        hashController.upgradeHashtags(poem.hashtags).catch((e) => {
            errorController.saveError("shatags.controller => upgrade", e.message);
        });
        return poem;
    }
}