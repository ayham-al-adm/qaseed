const poemModel = require('../models/poem.model');
const storyModel = require('../models/story.model');

const notificationController = require('./notification.controller');
const errorController = require('./error.controller');

module.exports = class Approving {
    static async rejectPoem(poemId, rejectMessage) {
        let poem = await poemModel.findById(poemId);
        poem.isApproved = rejectMessage;
        await poem.save();
        notificationController.addRejectPoemNotification(poem.owner, poemId)
            .catch((e) => {
                errorController.saveError("approving controller => reject notification", e.message);
            });
    }

    static async rejectStory(storyId, rejectMessage) {
        let story = await storyModel.findById(storyId);
        story.isApproved = rejectMessage;
        await story.save();
        notificationController.addRejectStoryNotification(story.owner, storyId)
            .catch((e) => {
                errorController.saveError("approving.controller => reject story notifiaction", e.message);
            });
    }
}