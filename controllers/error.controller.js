const errorModel = require('../models/error.model');

module.exports = class Error {
    static async saveError(file, message){
        let error = new errorModel({
            date: new Date,
            file: file,
            message: message
        });
        await error.save();
        return true;
    }
}