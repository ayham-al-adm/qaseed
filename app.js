const express = require('express');
const path = require('path');
const app = express();
const userRoutes = require('./routes/user.routes');
const otherRoutes = require('./routes/others.routes');
const poemRoutes = require('./routes/poem.routes');
const storyRoutes = require('./routes/story.routes');
const blockRoutes = require('./routes/block.routes');
const notificationRoutes = require('./routes/notifications.routes');
const hashRoutes = require('./routes/hashtags.routes');
const categoryRoutes = require('./routes/categories.routes');
const searchRoutes = require('./routes/search.routes');
const uploadsRoutes = require('./routes/uploads.routes');
const fakeUserRoutes = require('./routes/fake-user.routes');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use('/user', userRoutes);
app.use('/others', otherRoutes);
app.use('/poem', poemRoutes);
app.use('/story', storyRoutes);
app.use('/block', blockRoutes);
app.use('/notifications', notificationRoutes);
app.use('/hashtags', hashRoutes);
app.use('/categories', categoryRoutes);
app.use('/search', searchRoutes);
app.use('/uploads', uploadsRoutes);
app.use('/fake', fakeUserRoutes);

app.use('/', (req, res, next) => {
    res.status(404).sendFile(path.join(__dirname, "views", "home.html"));
});
module.exports = app;