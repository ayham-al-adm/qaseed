const mongoose = require('mongoose');

let schema = new mongoose.Schema({
    id: mongoose.ObjectId,
    date: Date,
    file: String,
    message: String
});

module.exports = mongoose.model('Error', schema);