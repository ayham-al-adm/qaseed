const mongoose = require('mongoose');


let schema = mongoose.Schema({
    id: mongoose.ObjectId,
    name: String,
    caption: String,
    date: Date,
    endDate: Date,
    likesNum: Number,
    isApproved: String,
    //category: { type: mongoose.ObjectId, ref: 'Category'},
    owner: {
        type: mongoose.ObjectId,
        ref: 'User'
    },
    verses: [{
        verse1: String,
        verse2: String,
    }],
    likes: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date
    }],
    backgroundImage: String,
});

module.exports = mongoose.model('Story', schema);