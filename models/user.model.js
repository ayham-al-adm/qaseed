const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const schema = mongoose.Schema({
    id: mongoose.ObjectId,
    email: {
        type: String,
    },
    password: String,
    verificationCode: String,
    verified: Boolean,
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    isPoet: Boolean,
    name: String,
    image: String,
    bio: String,
    birthdate: Date,
    country: String,
    city: String,
    otherAdress: String,
    language: String,
    isFake: Boolean,
    category: {
        type: mongoose.ObjectId,
        ref: 'Category'
    },
    followersNum: Number,
    followingNum: Number,
    poemsNum: Number,
    fcmToken: String,
    following: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date
    }],
    followers: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date
    }],
    blocking: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date
    }],
    blockers: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date
    }],
    verified: Boolean,
    faceId: String,
    faceToken: String,
    googleId: String,
    googleToken: String,
    appleId: String,
    appleToken: String,
    registerDate: Date,
    stories: [{
        name: String,
        caption: String,
        date: Date,
        endDate: Date,
        likesNum: Number,
        isApproved: String,
        //category: { type: mongoose.ObjectId, ref: 'Category'},
        verses: [{
            verse1: String,
            verse2: String,
        }],
        likes: [{
            user: {
                type: mongoose.ObjectId,
                ref: 'User'
            },
            date: Date
        }],
        backgroundImage: String,
    }],
});

schema.methods.generateAuthToken = async function () {
    const token = jwt.sign({
        '_id': this._id.toString(),
        'name': this.name
    }, process.env.JWT_KEY, {
        expiresIn: 60 /*seconds*/ * 60 /*minutes*/ * 24 /*hours*/ * 7/*days*/
    });

    this.tokens.concat({ token: token });
    await this.save();
    return token;
}


module.exports = mongoose.model('User', schema);