const mongoose = require('mongoose');

let schema = new mongoose.Schema({
    id: mongoose.ObjectId,
    n_type: String,
    nDate: Date,
    is_new: Boolean,
    user: {
        type: mongoose.ObjectId,
        ref: 'User'
    },
    poem: {
        type: mongoose.ObjectId,
        ref: 'Poem'
    },
    story: {
        type: mongoose.ObjectId,
        ref: 'Story'
    },
    owner: {
        type: mongoose.ObjectId,
        ref: 'User'
    },
    users: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User',
        },
        date: Date
    }]
});

module.exports = mongoose.model('Notification', schema);