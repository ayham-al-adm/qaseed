const mongoose = require('mongoose');

const schema = mongoose.Schema({
    id: mongoose.ObjectId,
    name: String,
    caption: String,
    date: Date,
    likesNum: Number,
    viewsCount: Number,
    versesType: String,
    isApproved: String,
    category: {
        type: mongoose.ObjectId,
        ref: 'Category'
    },
    owner: {
        type: mongoose.ObjectId,
        ref: 'User'
    },
    verses: [{
        verse1: String,
        verse2: String,
    }],
    likes: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date
    }],
    backgroundImage: String,
    backgroundColor: String,
    hashtags: [String],
    favoriteBy: [{
        user: {
            type: mongoose.ObjectId,
            ref: 'User'
        },
        date: Date,
    }]
});

module.exports = mongoose.model('Poem', schema);