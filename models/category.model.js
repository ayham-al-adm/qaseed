const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    id: mongoose.ObjectId,
    name: String,
    type: String,
    image: String
});


module.exports = mongoose.model('Category', schema);