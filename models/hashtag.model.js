const mongoose = require('mongoose');

let schema = mongoose.Schema({
    id: mongoose.ObjectId,
    text: String,
    usesCount: Number,
    lastUsed: Date
});

module.exports = mongoose.model('Hashtag', schema);