const axios = require('axios');

module.exports = class UserUtil {
    static async isFBUser(userID, accessToken) {
        let result;
        await axios.get(`https://graph.facebook.com/me?access_token=${accessToken}`)
            .then((rs) => {
                if (rs["status"] == 200) {
                    //true
                    //console.log(rs["data"]);
                    console.log('data');
                    result = rs['data']['id'];
                } else {
                    console.log('else');
                    result = false;
                }
            })
            .catch((e) => {
                result = false;
                console.log("ERROR AT GET USER DATA FROM FB");
            });
        //console.log(response);
        return result;
    }

    static async isGoogleUser(userID, accessToken) {
        try {
            let result;
            result = await axios.get(`https://oauth2.googleapis.com/tokeninfo?access_token=${accessToken}`)
            return result;
        } catch (e) {
            return false;
        }
    }
}