const mongoose = require('mongoose');
module.exports = (followingArray) => {
    let ids = Array();
    //ids.push(mongoose.Types.ObjectId("5f7c0ba1c586c52ed8f99b5f"));
    followingArray.forEach((follow) => {
        ids.push(mongoose.Types.ObjectId(follow.user.toString()));
    });
    return ids;
};