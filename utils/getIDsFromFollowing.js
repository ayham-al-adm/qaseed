module.exports = (followingArray) => {
    let ids = Array();
    followingArray.forEach((follow) => {
        ids.push(follow.user.toString());
    });
    return ids;
};