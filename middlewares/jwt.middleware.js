const JWT = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try{
        let token = req.headers.authorization.split(' ')[1];
        const decoded = JWT.verify(token, process.env.JWT_KEY);
        req.user = decoded;
        next();
    }catch (e) {
        if(e instanceof JWT.TokenExpiredError){
            res.status(401).json(e.message);
        }
        else if(e instanceof JWT.JsonWebTokenError){
            res.status(403).json(e.message);
        }
        else {
            return res.status(500).json("error in token");
        }
    }
}